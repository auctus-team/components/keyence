#include <keyence/keyence.hpp>

/**
 * \file keyence.ppc
 * \brief Drivers that get laser data from a Keyence SZV-32N range laser
 *        and publish it in a ros LaserScan message
 * \author Lucas Joseph
 * \version 0.1
 * \date 21/01/2020
 */


/**
 * \fn int main (void)
 * \brief Main function.
 *
 * \return EXIT_SUCCESS 
 */
int main (int  argc, char** argv)
{
    //Initializing ROS
    ros::init(argc, argv, "laser_scan_publisher");

    ros::NodeHandle n("~");
    std::string laser_topic = "scan"; /*!< @brief Name of the resulting laser topic  */
    if (!n.getParam("laser_topic", laser_topic)) {
        ROS_ERROR_STREAM("Could not read parameter laser_topic");
    }
    ros::Publisher scan_pub = n.advertise<sensor_msgs::LaserScan>(laser_topic, 50);
    ros::Rate r(1.0/0.04);

    //Initializing scanner message
    double laser_frequency = 40; /*!< @brief refreshing rate of the laser scanner (ms) */  
    unsigned int num_readings = 477; /*!< @brief Number of scan reading along one scan */  
    scan.header.frame_id = "laser"; /*!< @brief Frame id in the ros message */
    scan.angle_min = -5.2*M_PI/180; /*!< @brief Min scan angle */  
    scan.angle_max = 185.2*M_PI/180; /*!< @brief Max scan angle */
    scan.angle_increment = 0.4 * M_PI / 180;  /*!< @brief Angle increment */
    scan.time_increment = (1 / laser_frequency) / (num_readings); /*!< @brief Time increment */
    scan.range_min = 0.0; /*!< @brief Minimum laser distance */
    scan.range_max = 5.0; /*!< @brief Maximum laser distance */
    scan.ranges.resize(num_readings);
    scan.intensities.resize(num_readings);

    getDistance.Command = 0x90;  /*!< @brief Command 90 to get a measure */
    getDistance.ScanHeadNumber=0x01; /*!< @brief Scanner concerned  */
    getDistance.MeasurementDatasetNumber=0x00;   
    getDistance.MeasurementDatasetNumber=0x0000;
    // If the Start Beam, Number of Received Beams, and Skip Beams are all set to 0, all beams are received.
    getDistance.StartBeam=0x0000;
    getDistance.NumberOfReceivedBeams=0x0000;
    getDistance.SkipBeams=0x0000;
    
    
    //Initializing socket
    std::string host_name = "172.16.0.10"; /*!< @brief IP Adress of the host name  */
    if (!n.getParam("hostname", host_name)) {
        ROS_ERROR_STREAM("Could not read parameter host_name");
    }
    double port = 8800; /*!< @brief Port number of the host  */
    if (!n.getParam("port", port)) {
        ROS_ERROR_STREAM("Could not read parameter port");
    }

    host= (struct hostent *) gethostbyname((char *) host_name.c_str());
    ROS_INFO_STREAM("Waiting for host " << host_name << " on port " << port);

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    server_addr.sin_addr = *((struct in_addr *)host->h_addr);
    memset(&(server_addr.sin_zero),0,sizeof(server_addr.sin_zero));

    while(n.ok()){
        
        std::memcpy(getdistance,&getDistance,sizeof(getDistance));
        // Command to get the distance
        sendto(sock, getdistance, sizeof(getdistance), 0,
                    (struct sockaddr *)&server_addr, sizeof(struct sockaddr));

        ret = recvfrom(sock, buffer, BUFSIZE, 0, reinterpret_cast<sockaddr*>(&from), &fromlen);
        
        std::memcpy(&resp,buffer,sizeof(resp));
        
        // Treat data
        if (ret <= 0)
            ROS_INFO_STREAM( "Error " << ret );
        else
        {
            ROS_INFO_STREAM_ONCE("////////////////////////");
            ROS_INFO_STREAM_ONCE("// Keyence Laser Info //");
            ROS_INFO_STREAM_ONCE("////////////////////////");
            //Get information
            
            if (resp.Command != 0x90)
                ROS_ERROR_STREAM( " Wrong message sent " << unsigned(resp.Command));

                if (resp.Error == 0x02)
                ROS_ERROR_STREAM("Invalid scanner head number");
            else if (resp.Error ==0x03)
                ROS_ERROR_STREAM("Invalid parameters");
            else if (resp.Error ==0x04)
                ROS_ERROR_STREAM("Insufficient command length");
            else if (resp.Error ==0xff)
                ROS_ERROR_STREAM("Command analysis error");


            // Measurement Status
            if (resp.MeasurementStatus == 0x00)
                ROS_ERROR_STREAM("Measurement error");

            // Operating Mode
            if (resp.OperatingMode == 0x00)
                ROS_INFO_STREAM_ONCE("High Speed mode");
            else if (resp.OperatingMode == 0x01)
                ROS_INFO_STREAM_ONCE("Standard mode");


            ROS_INFO_STREAM_ONCE("Start Beam " << unsigned(resp.StartBeam));

            ROS_INFO_STREAM_ONCE("Number of Received Beams " << unsigned(resp.NumberOfReceivedBeams));

            ROS_INFO_STREAM_ONCE("Number of Skipped Beams " << unsigned(resp.SkipBeams) << "\n");

        }
        
        for (int i=0; i < nb_beams ; ++i)
        {
            std::memcpy(&dist_info,&resp.MeasurementData[i],sizeof(dist_info));
            scan.ranges[i] = (u_int16_t)dist_info.Distance.to_ulong()/1000.0;
            scan.intensities[i] = dist_info.ReflectiveObject[0];
        }
        
        scan.header.stamp = ros::Time::now();
        scan_pub.publish(scan);

        r.sleep();
    }
}
