#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <bitset>
#include <sstream>
#include "sensor_msgs/LaserScan.h"
#include <ros/ros.h>


#define BUFSIZE 968
#define AMPLITUDE 0.4
#define nb_beams 477

/**
\struct Command structure
*/
struct Command
{
    u_int8_t Command; 
    u_int8_t Empty = 0x0;
    u_int8_t ScanHeadNumber;
    u_int8_t MeasurementDatasetNumber;
    u_int16_t StartBeam;
    u_int16_t NumberOfReceivedBeams;
    u_int16_t SkipBeams;
};


/**
\struct Reply structure
*/
struct Reply
{
    u_int8_t Command;
    u_int8_t Error;
    u_int8_t ScanHeadNumber;
    u_int8_t MeasurementDatasetNumber;
    u_int8_t MeasurementStatus;
    u_int8_t OperatingMode;
    u_int8_t NumberOfScans;
    u_int8_t Empty=0x0;
    u_int16_t StartBeam;
    u_int16_t NumberOfReceivedBeams;
    u_int16_t SkipBeams;
    u_int16_t MeasurementData [nb_beams];
};

/**
\struct Distance info for ROS message
*/
struct DistanceInfo{
    std::bitset<15> Distance; // Distance to the laser frame
    std::bitset<1> ReflectiveObject; // 1 if the measured point is highly reflective
};

Reply resp;
DistanceInfo dist_info;
Command getDistance;

char getdistance [sizeof(getDistance)];

// Create UDP socket
int sock;
struct sockaddr_in server_addr;
struct hostent *host;
char buffer[BUFSIZE] = {0};
sockaddr_in from;
socklen_t fromlen = sizeof(from);;
int ret;
 
// Ros sensor message
sensor_msgs::LaserScan scan;
